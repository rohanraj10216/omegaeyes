package com.example.omegaeyes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraActivity;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends CameraActivity {

    private static final String TAG  = "MainActivity";

    private JavaCameraView mOpenCvCameraView;
    private boolean showContours = false;
    private ImageButton mCameraButton;

    BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
//                    mCameraButton.setVisibility(View.VISIBLE);
                } break;
                default:
                {
                    super.onManagerConnected(status);
//                    mCameraButton.setVisibility(View.GONE);
                } break;
            }
        }
    } ;


    static {
        if (OpenCVLoader.initDebug()) {
            Log.d(TAG,"initalised");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        if(OpenCVLoader.initDebug()){
//            Log.d("MainActivity","OpenCV initialised");
//            Toast.makeText(getApplicationContext(),"OpenCV init success",Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(getApplicationContext(),"OpenCV init failed",Toast.LENGTH_SHORT).show();
//        }
        mOpenCvCameraView = findViewById(R.id.openCvCameraView);
        mCameraButton = findViewById(R.id.cameraButton);
        mOpenCvCameraView.setVisibility(View.VISIBLE);
        mOpenCvCameraView.enableView();
        mOpenCvCameraView.setCvCameraViewListener(new CameraBridgeViewBase.CvCameraViewListener2() {
            @Override
            public void onCameraViewStarted(int width, int height) {

            }

            @Override
            public void onCameraViewStopped() {

            }

            @Override
            public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
                Log.i(TAG,"onCameraFrame");
                Mat src ;
                if(showContours){
                    src = inputFrame.rgba();
                    Mat gray = new Mat(src.rows(), src.cols(), src.type());
                    Imgproc.cvtColor(src, gray, Imgproc.COLOR_BGR2GRAY);
                    Mat binary = new Mat(src.rows(), src.cols(), src.type(), new Scalar(0));
                    Imgproc.threshold(gray, binary, 100, 255, Imgproc.THRESH_BINARY);
                    //Finding Contours
                    List<MatOfPoint> contoursList = new ArrayList<>();
                    Mat hierarchy = new Mat();
                    Imgproc.findContours(binary, contoursList, hierarchy, Imgproc.RETR_TREE,
                            Imgproc.CHAIN_APPROX_SIMPLE);


                    Scalar colorRed = new Scalar(255, 0, 0);
                    Scalar colorGreen = new Scalar(0, 255, 0);
                    for(int i=0; i<contoursList.size();i++){
                        double[] contour_info = hierarchy.get(0,i);
                        if (contour_info[contour_info.length-2]==-1 && contour_info[contour_info.length-1]==-1){
                            Imgproc.drawContours(src, contoursList, i,colorRed,3);
//                            Log.d("Bolt","Bolt contour is detected");
                        }
                        if (contour_info[contour_info.length-2]==-1 && contour_info[contour_info.length-1]!=-1){
                            Imgproc.drawContours(src, contoursList, i,colorGreen,3);
//                            Log.d("Nut","Nut contour is detected");
                        }
                    }

                    //Drawing the Contours

//                    Imgproc.drawContours(src, contoursList, -1, colorRed, 2, Imgproc.LINE_8,
//                            hierarchy, 2, new Point() ) ;

                    //Process to show contours
                }else{
                    src = inputFrame.rgba();
                }

                return src;
            }
        });
        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showContours = !showContours;
            }
        });

    }

    private Mat photo;

    @Override
    protected List<? extends CameraBridgeViewBase> getCameraViewList() {
        return Collections.singletonList(mOpenCvCameraView);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION,this, mLoaderCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
}